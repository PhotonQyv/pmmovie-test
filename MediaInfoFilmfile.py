# -*- coding: utf-8 -*-

from dataclasses import dataclass

"""
pmmovie-test - Simple toy python code to allow me to grab some
                 information from video files using pymediainfo output.

    Copyright (C) 2019  PhotonQyv

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""


# Class used to store the filenames got from the command-line.


@dataclass
class Filmfile:
    def __init__(self):

        self.filename: list[str] = ()
        self.debug: bool
